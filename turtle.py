import turtle

turtle.forward(10) # 10 moves forward
turtle.backward(10) # 10 moves backward
turtle.left(90) # rotates 90 degrees cursor left
turtle.right(90) # clockwise

turtle.penup() # cursor up
turtle.pendown() # cursor down