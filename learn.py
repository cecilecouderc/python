
# Adapter le code précédent pour afficher tous les nombres de 1 à 100.

for i in range(1, 101):
    print(i)

# Afficher un rectangle de taille 4x5 composé de "x".
ligne = "XXXXX"
for i in range (0, 4):
    rectangle = ''
    rectangle += ligne
    print(rectangle)

# Afficher tous les nombres de à 100 à 1.
i = 101
while (i > 1):
    i = i-1
    print(i)

# Afficher un triangle composé de 10 "o" au total.
for i in range(5):
    print("o" * i)

# Compléter ce code pour afficher les films diffusés après 12h :
horaires = [
    {'film': 'Seul sur Mars', 'heure': 9},
    {'film': 'Astérix et Obélix Mission Cléopâtre', 'heure': 10},
    {'film': 'Star Wars VII', 'heure': 15},
    {'film': 'Time Lapse', 'heure': 18},
    {'film': 'Fatal', 'heure': 20},
    {'film': 'Limitless', 'heure': 20},
]
for i in range (len(horaires)) :
    heure = horaires[i]['heure']
    if (heure > 12):
        print(horaires[i]['film'])
